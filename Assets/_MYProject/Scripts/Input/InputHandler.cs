using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace FXnRXn
{
    public class InputHandler : MonoBehaviour
    {
        #region VARIABLE
        [Header("Refferences :")]
        [SerializeField] private FixedJoystick _fixedJoystick;
        [SerializeField] private FixedTouchField _touchField;
        [Space(15)]
        //[SerializeField] private Transform _cameraTransform;
        //[SerializeField] private float _cameraSensitivity;
        
        private Vector2 _lookInput;
        private float _cameraPitch;
        
        
        public Controller _controller { get; private set; }

        #endregion
        
        
        
        
        #region UNITY METHOD

        private void Awake()
        {
            _controller = Object.FindObjectOfType<Controller>().GetComponent<Controller>();
        }

        private void Start()
        {

        }

        private void Update()
        {
            if(!_controller._isLocal)
                return;
            
            SetupControllerMovement();
            SetupCameraMovement();
           // Shooting();
        }

        #endregion

        private void SetupControllerMovement()
        {
            if(_controller == null || _fixedJoystick == null)
                return;
            
#if UNITY_ANDROID
            _controller.horizontal = _fixedJoystick.Horizontal;
            _controller.vertical = _fixedJoystick.Vertical;
#else
             _controller.horizontal = Input.GetAxis("Horizontal");
             _controller.vertical = Input.GetAxis("Vertical");
#endif
            
            // if (Application.platform == RuntimePlatform.WindowsEditor)
            // {
            //     _controller.horizontal = Input.GetAxis("Horizontal");
            //     _controller.vertical = Input.GetAxis("Vertical");
            // }
            // else
            // {
            //     _controller.horizontal = _fixedJoystick.Horizontal;
            //     _controller.vertical = _fixedJoystick.Vertical;
            // }
            
            
        }

        private void SetupCameraMovement()
        {
            if(_controller == null || _fixedJoystick == null)
                return;


#if UNITY_ANDROID
            _controller.mouseX = _touchField.TouchDist.x;
            _controller.mouseY = _touchField.TouchDist.y;

#else
             _controller.mouseX = Input.GetAxis("Mouse X");
             _controller.mouseY = Input.GetAxis("Mouse Y");
#endif

            // if (Application.platform == RuntimePlatform.WindowsEditor)
            // {
            //     _controller.mouseX = Input.GetAxis("Mouse X");
            //     _controller.mouseY = Input.GetAxis("Mouse Y");
            // }
            // else
            // {
            //     for (int i = 0; i < Input.touchCount; i++)
            //     {
            //         Touch t = Input.GetTouch(0);
            //
            //         switch (t.phase)
            //         {
            //             case TouchPhase.Moved:
            //                 _lookInput = t.deltaPosition;
            //                 break;
            //             case TouchPhase.Stationary:
            //                 _lookInput = Vector2.zero;
            //                 break;
            //             case TouchPhase.Ended:
            //                 _lookInput = Vector2.zero;
            //                 break;
            //         }
            //
            //         _controller.mouseX = _lookInput.x;
            //         _controller.mouseY = _lookInput.y;
            //     }
            // }



        }


        public void InputShooting(bool shooting)
        {
            if(_controller == null ) return;

            _controller._isShooting = shooting;

        }

        public void InputReloading(bool reloading)
        {
            if(_controller == null ) return;
            
            _controller._isReloading = reloading;
        }

    }
}


