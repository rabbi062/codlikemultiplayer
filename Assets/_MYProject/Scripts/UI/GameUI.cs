using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace FXnRXn
{
    public class GameUI : MonoBehaviour
    {
        #region Singleton

        public static GameUI singleton;

        private void Awake()
        {
            singleton = this;
        }

        #endregion

        #region Variable
        [Header("Ammo UI :")]
        [SerializeField] TMP_Text _bulletsStat;

        [Header("Interaction UI :")] 
        [SerializeField] GameObject _interactionObj;
        [SerializeField] TMP_Text _interactionText;
        [SerializeField] Slider _interactionSlider;

        #endregion

        #region Unity Method

        private Controller controller;

        public void Init(Controller c)
        {
            controller = c;
        }

        private void LateUpdate()
        {
            if(controller == null) return;
            
            string bullet = "<size=50>" + controller.m_currentWeapon.currentBullets +"<size=40>/"+ controller.m_currentWeapon.maxBulletsCarrying;
            _bulletsStat.text = bullet.ToString();

            if (controller.interactionAmount > 0)
            {
                _interactionObj.SetActive(true);
                _interactionText.text = controller.interactionString;
                _interactionSlider.maxValue = controller.interactionMaxAmount;
                _interactionSlider.value = controller.interactionAmount;
            }
            else
            {
                _interactionObj.SetActive(false);
            }
        }

        #endregion
        
        
        
    }
}


