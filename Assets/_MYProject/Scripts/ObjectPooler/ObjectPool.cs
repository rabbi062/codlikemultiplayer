using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FXnRXn.Utilities.ObjectPooler;

namespace FXnRXn.Utilities
{
    public static class ObjectPool
    {
        private static ObjectPoolAsset _poolAsset;

        public static ObjectPoolAsset ObjectPooler
        {
            get
            {
                if (_poolAsset == null)
                {
                    _poolAsset = Resources.Load("ObjectPooler") as ObjectPoolAsset;
                    _poolAsset.Init();
                }

                return _poolAsset;
            }
        }

        public static GameObject GetObject(string id)
        {
            return ObjectPooler.GetObject(id);
        }

    }
}

