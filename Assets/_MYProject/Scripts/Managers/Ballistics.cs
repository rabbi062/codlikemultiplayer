using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FXnRXn.Utilities;


namespace FXnRXn
{
    public static class Ballistics
    {
        public static void RaycastBullet(Vector3 origin, Vector3 direction, Controller owner)
        {
            //GameObject bulletGo = ObjectPool.GetObject("BulletLine");
            GameObject bulletGo = ObjectPool.GetObject("BulletLine");
            Vector3 hitPosition = origin + (direction * 100);

            RaycastHit hit;
            if (Physics.Raycast(origin, direction, out hit))
            {
                hitPosition = hit.point;
            }
            
            BulletLine bulletLine = bulletGo.GetComponent<BulletLine>();
            bulletLine.SetPositions(origin, hitPosition);
            bulletGo.SetActive(true);
        }
    }

}
