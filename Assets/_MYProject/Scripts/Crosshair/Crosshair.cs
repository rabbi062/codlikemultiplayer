using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FXnRXn
{
    public class Crosshair : MonoBehaviour
    {
        
        #region Singleton : Crosshair

        public static Crosshair singleton;

        private void Awake()
        {
            singleton = this;
            OnAwake();
        }

        #endregion
        
        #region VARIABLE
        
        [Header("Crosshair Setting :")]
        public float _targetSpread = 30;
        public float _maxSpread = 80;
        public float _spreadSpeed = 5;
        public float _defaultSpread;
        public float _smoothness;

        private float _curSpread;
        public float CurrentSpread
        {
            get
            {
                return _curSpread;
            }
        }
        
        public Parts[] m_parts;
        private float _t;
        
        [HideInInspector]
        public Controller m_controller;
        #endregion

        public void Init(Controller c)
        {
            m_controller = c;
        }

        public void OnAwake()
        {
            //LoadDefSpread(_defaultSpread);
        }
        
        
        #region UNITY METHOD

        private void FixedUpdate()
        {
            Tick(Time.fixedDeltaTime);
            
            if(m_controller == null)
                return;
            
            float moving = Mathf.Abs(m_controller.horizontal + m_controller.vertical + m_controller.mouseX + m_controller.mouseY);
            if (moving == 0)
            {
                _targetSpread = Mathf.Lerp(_targetSpread,_defaultSpread,  _smoothness) ;
            }
            AddSpread(moving);
        }

        #endregion

        #region PUBLIC

        public void Tick(float delta)
        {
            _t = delta * _spreadSpeed;

            if (_targetSpread > _maxSpread)
            {
                _targetSpread = _maxSpread;
            }
            
            _curSpread = Mathf.Lerp(_curSpread, _targetSpread, _t);
            for (int i = 0; i < m_parts.Length; i++)
            {
                Parts p = m_parts[i];
                p.trans.anchoredPosition = p.pos * _curSpread;
            }

        }

        public void AddSpread(float v)
        {
            _targetSpread = (_targetSpread + v);
        }

        // public void LoadDefSpread(float v)
        // {
        //     _defaultSpread = v * 100;
        // }

        #endregion
        

        [System.Serializable]
        public class Parts
        {
            public RectTransform trans;
            public Vector2 pos;
        }

    }
}


