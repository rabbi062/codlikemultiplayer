using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FXnRXn.Items;
using UnityEditor.MPE;


namespace FXnRXn
{
    [RequireComponent(typeof(CharacterController))]
    public class Controller : MonoBehaviour
    {

        #region VARIABLE

        public bool _isInteracting;
        public bool _isLocal = true;
        [Header("Settings :")] 
        public float _speed = 0.5f;
        public float _rotationSpeed = 0.5f;
        public bool _isShooting = false;
        public bool _isReloading = false;

        [Header("Camera :")] 
        public Transform _pivotTrans;
        public Transform _cameraTrans;

        [Space(10)] 
        public RuntimeItem m_currentWeapon;

        [Header("Recoil :")] 
        public Transform recoilTrans;

        private bool _recoilFlag;
        private float _curveT;
        private float _recoilValue;
        

        [HideInInspector] public float horizontal;
        [HideInInspector] public float vertical;
        [HideInInspector] public float mouseX;
        [HideInInspector] public float mouseY;
        [HideInInspector] public float lookAngle;
        [HideInInspector] public float tiltAngle;
        
        private CharacterController c;
        
        
        [HideInInspector] public float interactionAmount;
        [HideInInspector] public string interactionString;
        [HideInInspector] public float interactionMaxAmount;


        #endregion


        #region UNITY METHOD

        private void Start()
        {
            Init("TestWeapon"); 

            // if (Application.platform == RuntimePlatform.WindowsEditor)
            // {
            //     Cursor.lockState = CursorLockMode.Locked;
            //     Cursor.visible = false;
            // }
        }

        public void Init(string weaponId)
        {
            c = GetComponent<CharacterController>();

            if (_isLocal)
            {
                SetActiveController();
            }

            m_currentWeapon = ItemManager.singleton.CreateItemInstance(weaponId);
            //m_currentWeapon.Reload();
            FakeInteraction("Reloading",1, m_currentWeapon.Reload);
        }

        public void SetActiveController()
        {
            Crosshair.singleton.Init(this);
            GameUI.singleton.Init(this);
        }

        private void Update()
        {
            float delta = Time.deltaTime;
            HandleShooting();
            HandleRecoil(delta);
        }

        private void FixedUpdate()
        {
            float delta = Time.fixedDeltaTime;
            
            HandleMovement(delta);
            HandleRotation(delta);
        }

        #endregion
        
        
        //=========================
        private void HandleMovement(float delta)
        {
            Vector3 direction = transform.forward * vertical;
            direction += transform.right * horizontal;
            direction.Normalize();

            RaycastHit hit;
            Physics.SphereCast(transform.position, c.radius, Vector3.down, out hit, c.height / 2f, 
                Physics.AllLayers, QueryTriggerInteraction.Ignore);

            Vector3 desiredMove = Vector3.ProjectOnPlane(direction, hit.normal).normalized;

            if (!c.isGrounded)
            {
                desiredMove += Physics.gravity;
            }

            c.Move(desiredMove * (delta * _speed));
        }

        private void HandleRotation(float delta)
        {
            lookAngle += mouseX * (delta * _rotationSpeed);
            Vector3 camEulers = Vector3.zero;
            camEulers.y = lookAngle;
            transform.eulerAngles = camEulers;
            
            
            tiltAngle -= mouseY * (delta * _rotationSpeed);
            tiltAngle = Mathf.Clamp(tiltAngle, -45f, 45f);
            
            Vector3 tiltEuler = Vector3.zero;
            tiltEuler.x = tiltAngle;
            _pivotTrans.localEulerAngles = tiltEuler;
            
        }

        private void HandleShooting()
        {
            if(_isInteracting) return;

            if (_isShooting)
            {
                if(m_currentWeapon.CanFire())
                {
                    _recoilFlag = true;
                    m_currentWeapon.Shoot();
                    Ballistics.RaycastBullet(_cameraTrans.position, _cameraTrans.forward, this);
                }
            }
            else
            {
                if(_isReloading)
                {
                    FakeInteraction("Reloading",1, m_currentWeapon.Reload);
                    //m_currentWeapon.Reload();
                }
            }
        }

        private void HandleRecoil(float delta)
        {
            Quaternion targetRot = Quaternion.identity;
            float lerpSpeed = delta * m_currentWeapon.weapon.recoilResetSpeed;
            _curveT = Mathf.Clamp01(_curveT);
            float hRecoil = m_currentWeapon.weapon.recoilCurve.Evaluate(_curveT) *
                            m_currentWeapon.weapon.recoilMultiplier;

            float kickAmount = 0;
            Vector3 e = Vector3.zero;

            if (_recoilFlag)
            {
                // Vector3 e = Vector3.zero;
                // e.x = -_recoilValue;
                // e.y = hRecoil;
                // targetRot = Quaternion.Euler(e);

                lerpSpeed = delta / m_currentWeapon.weapon.recoilSpeed;
                _curveT += delta / m_currentWeapon.weapon.curveSpeed;

                if (_curveT > 1)
                {
                    _curveT = 0;
                }

                _recoilValue += m_currentWeapon.weapon.recoilvalue;
                kickAmount = _recoilValue;
                _recoilFlag = false;
                
                e.x = -kickAmount;
                e.y = hRecoil;
            }
            else
            {
                if (_curveT > 1)
                {
                    _curveT = 1;
                }
                _recoilValue = m_currentWeapon.weapon.recoilvalue;
                
                if (_curveT > 0)
                {
                    _curveT -= delta / m_currentWeapon.weapon.recoilResetSpeed;
                    
                }

                e.x = 0;
            }
            
            
            
            
            targetRot = Quaternion.Euler(e);
            
            recoilTrans.localRotation = Quaternion.Slerp(recoilTrans.localRotation, targetRot, lerpSpeed);
        }
        
        
        
        //==========================
        
        
        public delegate void OnFakeInteractionComplete();
        
        public void FakeInteraction(string interText, float amount, OnFakeInteractionComplete callback)
        {
            interactionString = interText;
            StartCoroutine(FakeInterRoutine(amount, callback));
        }

        IEnumerator FakeInterRoutine(float amount, OnFakeInteractionComplete callback)
        {
            _isInteracting = true;
            interactionAmount = amount;
            interactionMaxAmount = amount;
            
            while (interactionAmount > 0)
            {
                interactionAmount -= Time.deltaTime;
                yield return null;
            }
            _isInteracting = false;
            callback?.Invoke();
        }
        
        
    }
}


