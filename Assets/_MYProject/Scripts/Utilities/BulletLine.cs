using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FXnRXn
{
    public class BulletLine : MonoBehaviour
    {
        public LineRenderer lineRenderer;
        public float _speed = .2f;

        private void OnEnable()
        {
            lineRenderer.widthMultiplier = 1;
        }

        private void Update()
        {
            if (lineRenderer.widthMultiplier <= 0)
            {
                gameObject.SetActive(false);
            }
            else
            {
                lineRenderer.widthMultiplier -= Time.deltaTime / _speed;
            }
        }

        public void SetPositions(Vector3 startpoint, Vector3 endpoint)
        {
            lineRenderer.SetPosition(0, startpoint);
            lineRenderer.SetPosition(1, endpoint);
        }
    }
}

