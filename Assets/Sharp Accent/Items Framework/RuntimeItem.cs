﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FXnRXn.Items
{
	public class RuntimeItem 
	{
		public string instanceId;
		public Item baseItem;

		public Weapon weapon
		{
			get
			{
				return (Weapon)baseItem;
			}
		}

		public int currentBullets;
		public int maxBulletsCarrying;

		private float lastFired;

		public RuntimeItem(Item baseItem)
		{
			this.baseItem = baseItem;
			if (baseItem is Weapon)
			{
				InitWeapon();
			}
		}


		public void InitWeapon()
		{
			maxBulletsCarrying = weapon.maxBullets;
		}

		public bool CanFire()
		{
			if (currentBullets > 0)
			{
				return (Time.realtimeSinceStartup - lastFired) > weapon.fireRate;
			}
			return false;
		}

		public void Shoot()
		{
			lastFired = Time.realtimeSinceStartup;
			currentBullets--;
		}


		public void Reload()
		{
			//Weapon w = (Weapon) baseItem;
			//currentBullets = w.magazineBullets;

			if (maxBulletsCarrying >= weapon.magazineBullets)
			{
				currentBullets = weapon.magazineBullets;
				maxBulletsCarrying -= weapon.magazineBullets;
			}
			else
			{
				//int dif = weapon.magazineBullets - maxBulletsCarrying;
				currentBullets = maxBulletsCarrying;
				maxBulletsCarrying = 0;
			}
		}
	}
}
