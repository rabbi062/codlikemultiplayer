﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FXnRXn.Items
{
	public abstract class Item : ScriptableObject
	{
		public ItemType itemType;
		public GameObject prefab;
	}
}
