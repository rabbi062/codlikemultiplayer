﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FXnRXn.Items
{
	[CreateAssetMenu(menuName ="Items/Weapon")]
	public class Weapon : Item
	{
		public float fireRate = 0.1f;
		public int magazineBullets = 20;
		public int maxBullets = 120;

		public float recoilResetSpeed = 3;
		public AnimationCurve recoilCurve;
		public float recoilMultiplier = 1;
		public float recoilvalue;
		public float recoilSpeed;
		public float curveSpeed;

	}
}
